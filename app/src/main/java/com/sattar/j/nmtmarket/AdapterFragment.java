//package com.sattar.j.nmtmarket;
//
//import android.support.annotation.Nullable;
//
//import android.support.v4.app.Fragment;
//import android.support.v4.app.FragmentManager;
//import android.support.v4.app.FragmentPagerAdapter;
//import android.support.v4.app.FragmentStatePagerAdapter;
//import android.support.v4.view.PagerAdapter;
//
//class AdapterFragment extends FragmentPagerAdapter {
//
//    final int PAGE_COUNT = 3 ;
//
//    private String tabTitles[] = new String[]{"مورد علاقه","پیامک","دیجیتال مارکتینگ"};
//
//    public AdapterFragment(FragmentManager fm) {
//        super(fm);
//    }
//
//    @Override
//    public Fragment getItem(int position) {
//        return PageFragment.newInstance();
//
//    }
//
//    @Override
//    public int getCount() {
//        return PAGE_COUNT;
//    }
//
//
//    @Override
//    public CharSequence getPageTitle(int position) {
//        return tabTitles[position];
//    }
//}
