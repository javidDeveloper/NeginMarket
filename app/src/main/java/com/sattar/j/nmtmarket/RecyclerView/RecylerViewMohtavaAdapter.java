package com.sattar.j.nmtmarket.RecyclerView;

//import android.support.v7.app.AlertController;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.sattar.j.nmtmarket.InnerPage.InnerPageActivity;
import com.sattar.j.nmtmarket.Models.ProductModels;
import com.sattar.j.nmtmarket.PublicMethods;
import com.sattar.j.nmtmarket.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class RecylerViewMohtavaAdapter extends RecyclerView.Adapter<RecylerViewMohtavaAdapter.MyViewHolder> {
    Context mContext;
    List<ProductModels> models;
    View view;


    public RecylerViewMohtavaAdapter() {
    }

    public RecylerViewMohtavaAdapter(Context mContext, List<ProductModels> models) {
        this.mContext = mContext;
        this.models = models;

    }

    @Override
    public RecylerViewMohtavaAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = LayoutInflater.from(mContext).inflate(R.layout.adapter_card_view_mohtava, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecylerViewMohtavaAdapter.MyViewHolder holder, final int position) {

        holder.description.setText(models.get(position).getDescription());
        holder.Discount.setText(models.get(position).getDiscount());
        holder.serviceName.setText(models.get(position).getServiceName());
//        holder.price.setText(models.get(position).getDiscount());
//        holder.Discount.setText(models.get(position).getPrice());
        double priceVal = Double.parseDouble(models.get(position).getPrice());
        double discountVal = Double.parseDouble(models.get(position).getDiscount());
        if (priceVal > discountVal) {
            holder.price.setText(PublicMethods.currencyToman(models.get(position).getDiscount()) + " " + mContext.getString(R.string.toman));
            holder.Discount.setText(PublicMethods.currencyToman(models.get(position).getPrice()) + " " + mContext.getString(R.string.toman));

        } else {
            holder.price.setText(PublicMethods.currencyToman(models.get(position).getPrice()) + " " + mContext.getString(R.string.toman));
            holder.Discount.setText("");
            holder.Discount.setVisibility(View.GONE);

        }
        try {
            Picasso.with(mContext).load(models.get(position).getAvatarURL()).into(holder.avatar);
        } catch (Exception e) {
            e.printStackTrace();
        }

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String title = mContext.getString(R.string.mohtavaString);
                goToExtra(mContext, InnerPageActivity.class, title, "id", String.valueOf(position + 1));


            }
        });
    }

    private void goToExtra(Context mContext, Class Destination, String title, String key, String value) {
        Intent intent = new Intent(mContext, Destination);
        intent.putExtra("title", title);
        intent.putExtra(key, value);
        mContext.startActivity(intent);
        Log.i("InnerPageTestValue1", title + value);
    }


    @Override
    public int getItemCount() {
        return models.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView avatar;
        TextView description;
        TextView Discount;
        TextView price;
        TextView serviceName;
        CardView cardView;

        public MyViewHolder(View itemView) {
            super(itemView);
            avatar = itemView.findViewById(R.id.avatar);
            serviceName = itemView.findViewById(R.id.serviceName);
            description = itemView.findViewById(R.id.description);
            Discount = itemView.findViewById(R.id.Discount);
            price = itemView.findViewById(R.id.price);
            cardView = itemView.findViewById(R.id.cardView);
        }
    }
}
