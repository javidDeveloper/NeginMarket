package com.sattar.j.nmtmarket.Registering;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.sattar.j.nmtmarket.MainActivity;
import com.sattar.j.nmtmarket.MyWidget.MyEditText;
import com.sattar.j.nmtmarket.MyWidget.MyTextView;
import com.sattar.j.nmtmarket.PublicMethods;
import com.sattar.j.nmtmarket.R;
import com.sattar.j.nmtmarket.ShopActivity;
import com.squareup.picasso.Picasso;

public class InputCodeActivity extends AppCompatActivity implements View.OnClickListener {
    MyTextView title,mobileTxt;
    MyEditText RegisterCode;
    ImageView close;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_code);
        onBind();
        onOparation();
    }



    private void onBind() {
        title = findViewById(R.id.title);
        mobileTxt = findViewById(R.id.mobileTxt);
        close = findViewById(R.id.back);
        RegisterCode = findViewById(R.id.RegisterCode);
        findViewById(R.id.Login).setOnClickListener(this);
        findViewById(R.id.Basket).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();

        if (id == R.id.Login) {
            if(RegisterCode.getText().toString().length()<4){
                PublicMethods.toast(this,getString(R.string.errorCode));
            }
            if(RegisterCode.getText().toString().equals("1234")){
                PublicMethods.goToWhithOneKey(this,MainActivity.class,"RegisterNumber","true");
                PublicMethods.toast(this,getString(R.string.yourNumberAccepted));
                finish();
            }
            else {
                PublicMethods.toast(this,getString(R.string.errorCode));
            }
        }

        if (id == R.id.Basket) {
            PublicMethods.goTo(this,ShopActivity.class);
        }
    }


    private void onOparation() {
        Picasso.with(this).load(R.drawable.close).into(close);
        title.setText(R.string.VerifyPhoneNumber);
        Intent intent = getIntent();
        String Value = intent.getStringExtra("mobile");
        mobileTxt.setText(Value);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {

    }
}
