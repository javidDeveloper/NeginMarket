//package com.sattar.j.nmtmarket;
//
//import android.content.Context;
//import android.content.Intent;
//import android.support.v7.widget.RecyclerView;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//
//import com.sattar.j.nmtmarket.InnerPage.InnerPageActivity;
//import com.sattar.j.nmtmarket.MyWidget.MyTextView;
//import com.sattar.j.nmtmarket.ViewHolders.ViewHolderMohtava;
//import com.sattar.j.nmtmarket.demo.InitCommunication;
//import com.squareup.picasso.Picasso;
//
//public class MohtavaAdapter extends RecyclerView.Adapter<ViewHolderMohtava> {
//    Context context;
//    LayoutInflater inflater;
//    ImageView avatar;
//    MyTextView title, description, price;
//    LinearLayout cardAdapter;
//
//    public MohtavaAdapter(Context context) {
//        this.context = context;
//        this.inflater = inflater.from(context);
//    }
//
//    @Override
//    public ViewHolderMohtava onCreateViewHolder(ViewGroup parent, int viewType) {
//        View view = inflater.inflate(R.layout.adapter_card_view_mohtava, parent, false);
//        avatar = view.findViewById(R.id.avatar);
//        title = view.findViewById(R.id.title);
//        description = view.findViewById(R.id.description);
//        price = view.findViewById(R.id.price);
//        ViewHolderMohtava viewHolderMohtava = new ViewHolderMohtava(view);
//        return viewHolderMohtava;
//    }
//
//    @Override
//    public void onBindViewHolder(ViewHolderMohtava holder, int position) {
//
//        holder.titleServiceName.setText(InitCommunication.getProduct().get(position).getServiceName());
//        holder.description.setText(InitCommunication.getProduct().get(position).getNumber());
////        holder.time.setText(InitCommunication.getProduct().get(position).getTime());
//        holder.price.setText(InitCommunication.getProduct().get(position).getPrice());
//        Picasso.with(context).load(InitCommunication.getProduct().get(position).getAvatarURL()).into(avatar);
//        holder.cardAdapter.setOnClickListener(clicklistener);
//        holder.cardAdapter.setId(position);
//    }
//
//    View.OnClickListener clicklistener = new View.OnClickListener() {
//        @Override
//        public void onClick(View v) {
//            Intent intent = new Intent(MainActivity.context, InnerPageActivity.class);
//
//            MainActivity.context.startActivity(intent);
//        }
//    };
//
//    @Override
//    public int getItemCount() {
//        return InitCommunication.getProduct().size();
//    }
//}
