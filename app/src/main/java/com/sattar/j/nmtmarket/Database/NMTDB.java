package com.sattar.j.nmtmarket.Database;


import com.orm.SugarRecord;

public class NMTDB extends SugarRecord<NMTDB> {
    private String Name;
    private String Email;
    private String Tel;
    private String Mobile;
    private String UserName;
    private String Password;
    private String Address;
    private String AddressChaenl;
    private String AddressInstagram;

    public NMTDB() {
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getTel() {
        return Tel;
    }

    public void setTel(String tel) {
        Tel = tel;
    }

    public String getMobile() {
        return Mobile;
    }

    public void setMobile(String mobile) {
        Mobile = mobile;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getAddressChaenl() {
        return AddressChaenl;
    }

    public void setAddressChaenl(String addressChaenl) {
        AddressChaenl = addressChaenl;
    }

    public String getAddressInstagram() {
        return AddressInstagram;
    }

    public void setAddressInstagram(String addressInstagram) {
        AddressInstagram = addressInstagram;
    }
}
