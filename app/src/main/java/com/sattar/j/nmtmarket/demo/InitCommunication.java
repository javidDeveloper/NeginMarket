package com.sattar.j.nmtmarket.demo;

import android.app.Application;

import com.sattar.j.nmtmarket.Models.EmailModels;
import com.sattar.j.nmtmarket.GroupingPages.Models.GroupingModel;
import com.sattar.j.nmtmarket.Models.MessengersModels;
import com.sattar.j.nmtmarket.Models.PocketModels;
import com.sattar.j.nmtmarket.Models.ProductModels;

import java.util.ArrayList;
import java.util.List;

public class InitCommunication extends Application {
    public static boolean pauseHappend = false;
    public static boolean pauseHappendFragment = false;
    public static int sessionId;
    public static List<MessengersModels> messengers = new ArrayList<>();
    public static List<ProductModels> mohtava = new ArrayList<>();
    public static List<EmailModels> email = new ArrayList<>();
    public static List<PocketModels> pocket = new ArrayList<>();
    public static List<GroupingModel> grouping = new ArrayList<>();


    public InitCommunication() {
    }


    public InitCommunication(int sessionId) {
        this.sessionId = sessionId;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        //TODO Getting server init data
    }

    public static int getSessionId() {
        return sessionId;
    }


    public static void setSessionId(int sessionId) {
        InitCommunication.sessionId = sessionId;
    }

    //////////////////////Messenger
    public static void addMessenger(MessengersModels messenger) {
        messengers.add(messenger);
    }

    public static List<MessengersModels> getMessengers() {
        return messengers;
    }


    public static void setMessengers(List<MessengersModels> messengers) {
        InitCommunication.messengers = messengers;
    }

    //////////////////////Mohtava
    public static void addMohtava(ProductModels product) {
        mohtava.add(product);
    }

    public static List<ProductModels> getProduct() {
        return mohtava;
    }

    public static void setMohtava(List<ProductModels> product) {
        InitCommunication.mohtava = product;
    }

    //////////////////////Email
    public static void addEmail(EmailModels emailModels) {
        email.add(emailModels);
    }

    public static List<EmailModels> getEmail() {
        return email;
    }


    public static void setEmail(List<EmailModels> emailModels) {
        InitCommunication.email = emailModels;
    }

    //////////////////////Pocket
    public static void addPocket(PocketModels pocketmodels) {
        pocket.add(pocketmodels);
    }

    public static List<PocketModels> getPocket() {
        return pocket;
    }


    public static void setPocket(List<PocketModels> pocketmodels) {
        InitCommunication.pocket = pocketmodels;
    }

    //////////////////////Grouping
    public static void addGrouping(GroupingModel groupingModel) {
        grouping.add(groupingModel);
    }

    public static List<GroupingModel> getGrouping() {
        return grouping;
    }

    public static void setGrouping(List<GroupingModel> grouping) {
        InitCommunication.grouping = grouping;
    }
}