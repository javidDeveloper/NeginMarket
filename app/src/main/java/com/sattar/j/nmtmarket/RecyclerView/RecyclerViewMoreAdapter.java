package com.sattar.j.nmtmarket.RecyclerView;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.sattar.j.nmtmarket.Models.MoreModels;
import com.sattar.j.nmtmarket.MyWidget.MyTextView;
import com.sattar.j.nmtmarket.R;
import com.squareup.picasso.Picasso;

import java.util.List;


public class RecyclerViewMoreAdapter extends RecyclerView.Adapter<RecyclerViewMoreAdapter.MyViewHolder> {
    private Context mContext;
    private List<MoreModels> models;
    private View view;




    public RecyclerViewMoreAdapter(Context mContext, List<MoreModels> models) {
        this.mContext = mContext;
        this.models = models;

    }

    @Override
    public RecyclerViewMoreAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = LayoutInflater.from(mContext).inflate(R.layout.adapter_card_view_mohtava, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerViewMoreAdapter.MyViewHolder holder, int position) {

        holder.description.setText(models.get(position).getDescription());
        Picasso.with(mContext).load(models.get(position).getAvatarURL()).into(holder.avatar);


    }


    @Override
    public int getItemCount() {
        return models.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView avatar;
        MyTextView description;
        MyTextView Discount;
        MyTextView price;


        public MyViewHolder(View itemView) {
            super(itemView);
            avatar = itemView.findViewById(R.id.avatar);
            description = itemView.findViewById(R.id.description);
            Discount = itemView.findViewById(R.id.Discount);
            price = itemView.findViewById(R.id.price);

        }
    }
}
