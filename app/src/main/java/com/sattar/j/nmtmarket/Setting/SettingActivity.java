package com.sattar.j.nmtmarket.Setting;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.rey.material.widget.Slider;
import com.sattar.j.nmtmarket.MyWidget.MyTextView;
import com.sattar.j.nmtmarket.R;

public class SettingActivity extends AppCompatActivity {
    Slider slider;
    MyTextView textView;
    String fontSize;

    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        onBind();
        sliderTools();
        demoTxtsize();
    }


    private void onBind() {
        slider = findViewById(R.id.slider);
        textView = findViewById(R.id.txtDemo);
    }

    private void sliderTools() {
        slider.setOnPositionChangeListener(new Slider.OnPositionChangeListener() {
            @Override
            public void onPositionChanged(Slider view, boolean fromUser, float oldPos, float newPos, int oldValue, int newValue) {
                Log.i("LOG", newValue + "");
                textView.setTextSize((float) newValue);
                fontSize = newValue + "";

            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        PreferenceManager.getDefaultSharedPreferences(this).edit()
                .putString("fontSize", fontSize).apply();
    }

    private void demoTxtsize() {
        String size = PreferenceManager.getDefaultSharedPreferences(this)
                .getString("fontSize", "20");
        int txtSize = Integer.parseInt(size);
        textView.setTextSize(txtSize);
        slider.setValue(txtSize,true);
    }
}
