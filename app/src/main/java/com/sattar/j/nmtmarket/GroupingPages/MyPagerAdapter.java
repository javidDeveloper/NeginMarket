package com.sattar.j.nmtmarket.GroupingPages;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.sattar.j.nmtmarket.GroupingPages.Fragments.EmailBankFragment;
import com.sattar.j.nmtmarket.GroupingPages.Fragments.PolymerPocketFragment;
import com.sattar.j.nmtmarket.GroupingPages.Fragments.ProductContentFragment;
import com.sattar.j.nmtmarket.GroupingPages.Fragments.SocialNetworksFragment;

public class MyPagerAdapter extends FragmentPagerAdapter {

    public MyPagerAdapter(FragmentManager fm, Context mContext) {
        super(fm);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return 4;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return ProductContentFragment.newInstance();
            case 1:
                return EmailBankFragment.newInstance();
            case 2:
                return SocialNetworksFragment.newInstance();
            case 3:
                return PolymerPocketFragment.newInstance();
            default:
                return null;
        }
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "تولید محتوا";
            case 1:
                return "بانک ایمیل مشاغل";
            case 2:
                return "شبکه های اجتماعی";
            case 3:
                return "پاکت پلیمری";
            default:
                return null;
        }
    }
}
