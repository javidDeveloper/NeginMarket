package com.sattar.j.nmtmarket.InnerPage;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.sattar.j.nmtmarket.Models.ProductModels;
import com.sattar.j.nmtmarket.MyWidget.MyTextView;
import com.sattar.j.nmtmarket.PublicMethods;
import com.sattar.j.nmtmarket.R;
import com.sattar.j.nmtmarket.ShopActivity;
import com.sattar.j.nmtmarket.demo.InitCommunication;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.Objects;

public class InnerPageActivity extends AppCompatActivity implements View.OnClickListener {
    MyTextView contentDescription, toolbarTitle, price, priceDiscount, fullTitle;
    ImageView avatar,like;
    int txtSize;
    public String title;
    public String id;
    Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inner_page);
        onBind();
        sharePreferenceTools();
        getExtra();
    }

    private void onBind() {
        contentDescription = findViewById(R.id.content_description);
        toolbarTitle = findViewById(R.id.title);
        fullTitle = findViewById(R.id.fullTitle);
        avatar = findViewById(R.id.avatar);
        price = findViewById(R.id.price);
        priceDiscount = findViewById(R.id.priceDiscount);
        like = findViewById(R.id.like);
        findViewById(R.id.addToBasket).setOnClickListener(this);
        findViewById(R.id.back).setOnClickListener(this);
        findViewById(R.id.copy).setOnClickListener(this);
        findViewById(R.id.share).setOnClickListener(this);
        findViewById(R.id.email).setOnClickListener(this);
        findViewById(R.id.Basket).setOnClickListener(this);
    }

    private void sharePreferenceTools() {
        String size = PreferenceManager.getDefaultSharedPreferences(this)
                .getString("fontSize", "15");
        txtSize = Integer.parseInt(size);
        contentDescription.setTextSize(txtSize);

    }

    private void getExtra() {
        Intent intent = getIntent();
        title = intent.getStringExtra("title");
        id = intent.getStringExtra("id");
        if (title.equals(getString(R.string.mohtavaString))) {
            setMohtava();
            return;
        }
    }

    private void setMohtava() {
        List<ProductModels> product = InitCommunication.getProduct();
        for (ProductModels productModels : product) {
            toolbarTitle.setText(productModels.getServiceName());
            fullTitle.setText(productModels.getServiceName());
            contentDescription.setText(productModels.getDescriptionFull());
            Picasso.with(mContext).load(productModels.getAvatarURL()).into(avatar);
            double priceVal = Double.parseDouble(productModels.getPrice());
            double discountVal = Double.parseDouble(productModels.getDiscount());
            if (priceVal > discountVal) {
                price.setText(PublicMethods.currencyToman(productModels.getDiscount()) + " " + getString(R.string.toman));
                priceDiscount.setText(PublicMethods.currencyToman(productModels.getPrice()) + " " + getString(R.string.toman));

            } else {
                price.setText(PublicMethods.currencyToman(productModels.getPrice()) + " " + getString(R.string.toman));
                priceDiscount.setText("");
            }
        }

    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.copy) {
            ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
            ClipData clip = ClipData.newPlainText("lable", toolbarTitle.getText().toString()
                    + "\n" + "\n" + contentDescription.getText().toString() + "\n" + "\n" + getString(R.string.BannerNeginMarMar)
                    + "\n" + getString(R.string.MyTel)
                    + "\n" + getString(R.string.telegramLink));
            clipboard.setPrimaryClip(clip);
            PublicMethods.snack(getString(R.string.copyToClipBoard), view);
        }
        if (id == R.id.share) {
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT, toolbarTitle.getText().toString()
                    + "\n" + "\n" + contentDescription.getText().toString() + "\n" + "\n" + getString(R.string.BannerNeginMarMar)
                    + "\n" + getString(R.string.MyTel)
                    + "\n" + getString(R.string.telegramLink));
            sendIntent.setType("text/plain");
            startActivity(sendIntent);
        }
        if (id == R.id.email) {
            try {
                Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                        "mailto", getString(R.string.myEmail), null));
                intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.subjectShareString));
                intent.putExtra(Intent.EXTRA_TEXT, toolbarTitle.getText().toString()
                        + "\n" + "\n" + contentDescription.getText().toString() + "\n" + "\n" + getString(R.string.BannerNeginMarMar)
                        + "\n" + getString(R.string.MyTel)
                        + "\n" + getString(R.string.telegramLink));
                startActivity(intent);
            }catch (Exception e){
                PublicMethods.snack(getString(R.string.NotFoundEmailApp),view);
            }
        }
        if (id == R.id.like) {
            PublicMethods.toast(mContext,getString(R.string.addToBasket));
        }
        if (id == R.id.addToBasket) {
            PublicMethods.toast(mContext,getString(R.string.addToBasket));
        }
        if (id == R.id.back) {
            finish();
        }
        if (id == R.id.Basket) {
            PublicMethods.moveToActivity(this, ShopActivity.class);
        }
    }
}
