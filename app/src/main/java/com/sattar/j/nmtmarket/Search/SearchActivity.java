package com.sattar.j.nmtmarket.Search;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;

import com.sattar.j.nmtmarket.BaseActivity;
import com.sattar.j.nmtmarket.MyWidget.MyEditText;
import com.sattar.j.nmtmarket.PublicMethods;
import com.sattar.j.nmtmarket.R;

public class SearchActivity extends BaseActivity implements View.OnClickListener {
    MyEditText EditSearch;
    ImageView BtnSearch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        onBind();
    }

    private void onBind() {
        EditSearch = findViewById(R.id.EditSearch);
        BtnSearch = findViewById(R.id.BtnSearch);
        BtnSearch.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.BtnSearch) {
            keyBoardCheck();
            if (EditSearch.length() == 0) {
                PublicMethods.snack(getString(R.string.emptyEditText), v);
            } else {

            }
        }
    }

    private void keyBoardCheck() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }
}
