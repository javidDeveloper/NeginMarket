package com.sattar.j.nmtmarket;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;
import com.sattar.j.nmtmarket.GroupingPages.ProductCategorizationActivity;
import com.sattar.j.nmtmarket.Models.EmailModels;
import com.sattar.j.nmtmarket.Models.MessengersModels;
import com.sattar.j.nmtmarket.Models.PocketModels;
import com.sattar.j.nmtmarket.Models.ProductModels;
import com.sattar.j.nmtmarket.RecyclerView.RecyclerViewMohtavaAdapter;
import com.sattar.j.nmtmarket.Registering.LoginActivity;
import com.sattar.j.nmtmarket.Search.SearchActivity;
import com.sattar.j.nmtmarket.Setting.SettingActivity;
import com.sattar.j.nmtmarket.demo.InitCommunication;

import cz.msebera.android.httpclient.Header;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    DrawerLayout drawerLayout;
    NavigationView navigationView;
    RecyclerView myRecycleMohtava;
    ImageView Register;
    InitCommunication communication = ((InitCommunication) this.getApplication());
    boolean backed = false;
    public static Context context;

    @Override
    protected void onResume() {
        super.onResume();
        InitCommunication.pauseHappend = true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.navigation_activity_main);
        context = getApplicationContext();
        onBind();

        onRegister();
        if (!InitCommunication.pauseHappend) {
            selectMohtava();



            selectMassengers();
            selectEmail();
            selectPocket();
        }
    }

    private void onRegister() {
//        PreferenceManager.getDefaultSharedPreferences(this).edit()
//                .putString("fontSize", fontSize).apply();
        Intent intent = getIntent();
        String mobile = intent.getStringExtra("RegisterNumber");
        if (!mobile.equals("true")) {
            Register.setVisibility(View.GONE);
        } else {
            Register.setVisibility(View.VISIBLE);
        }
    }


    private void onBind() {
        drawerLayout = findViewById(R.id.drawerLayout);
        Register = findViewById(R.id.Register);
        navigationView = findViewById(R.id.navigationView);
        myRecycleMohtava = findViewById(R.id.myRecycleMohtava);
        findViewById(R.id.search).setOnClickListener(this);
        findViewById(R.id.hamburger).setOnClickListener(this);
        findViewById(R.id.Basket).setOnClickListener(this);
        navigationView.inflateHeaderView(R.layout.header_layout);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {
                ////////////////
                int id = item.getItemId();
                if (id == R.id.GroupingProduct) {
                    PublicMethods.goToWhithOneKey(context, ProductCategorizationActivity.class, "title",
                            getString(R.string.GroupingProduct));
                    PublicMethods.drawerCheck(drawerLayout);
                }
                if (id == R.id.register) {
                    PublicMethods.moveToActivity(context, LoginActivity.class);
                    PublicMethods.drawerCheck(drawerLayout);
                }
                if (id == R.id.searchMenu) {
                    PublicMethods.moveToActivity(context, SearchActivity.class);
                    PublicMethods.drawerCheck(drawerLayout);
                }

                if (id == R.id.shopMenu) {
                    PublicMethods.moveToActivity(context, ShopActivity.class);
                    PublicMethods.drawerCheck(drawerLayout);
                }
                if (id == R.id.settingMenu) {
                    PublicMethods.moveToActivity(context, SettingActivity.class);
                    PublicMethods.drawerCheck(drawerLayout);
                }
                if (id == R.id.aboutMenu) {
                    PublicMethods.toast(context, getString(R.string.about));
                    PublicMethods.drawerCheck(drawerLayout);
                }
                return true;
            }
        });
    }

    public void adapterRecyclerView() {
        RecyclerViewMohtavaAdapter adapter = new RecyclerViewMohtavaAdapter(context, InitCommunication.getProduct());
        myRecycleMohtava.setAdapter(adapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, true);
        myRecycleMohtava.setLayoutManager(layoutManager);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.hamburger) {
            drawerLayout.openDrawer(Gravity.RIGHT);
        }
        if (id == R.id.Basket) {
            PublicMethods.moveToActivity(context, ShopActivity.class);
        }
        if (id == R.id.search) {
            PublicMethods.toast(context, getString(R.string.search));
            PublicMethods.moveToActivity(context, SearchActivity.class);
        }
    }


    @Override
    public void onBackPressed() {
        PublicMethods.drawerCheck(drawerLayout);
        if (backed)
            finish();
        PublicMethods.toast(context, getString(R.string.backAgain));
        backed = true;
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                backed = false;
            }
        }, 1500);
    }


    private void selectMohtava() {
        String link = getString(R.string.DomainString) + "5ac48e4e2f00002a00f5fa32";
        mohtavaParserAndSet(link);
    }

    private void selectMassengers() {
        String link = getString(R.string.DomainString) + "5a9d5c4a310000491dab53ca";
        massengersParserAndSet(link);
    }

    private void selectEmail() {
        String link = getString(R.string.DomainString) + "5a9f8c472e00002a0074d0d2";
        emailParserAndSet(link);
    }

    private void selectPocket() {
        String link = getString(R.string.DomainString) + "5a9f950b2e00005a0074d0ec";
        pocketParserAndSet(link);
    }


    public void mohtavaParserAndSet(String link) {
        AsyncHttpClient client = new AsyncHttpClient();

        client.post(link, new TextHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                PublicMethods.toast(context, getString(R.string.searchNotFound));
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                Gson gson = new Gson();
                ProductModels models = gson.fromJson(responseString, ProductModels.class);
                int id = models.getId();
                String Title = models.getTitle();
                String ServiceName = models.getServiceName();
                String Description = models.getDescription();
                String DescriptionFull = models.getDescriptionFull();
                String Discount = models.getDiscount();
                String Number = models.getNumber();
                String Time = models.getTime();
                String Price = models.getPrice();
                String Avatar = models.getAvatarURL();
                Log.e("GSONTEST", "Title : " + Title + "ServiceName : " + ServiceName +
                        "Description : " + Description + "DescriptionFull : " + DescriptionFull +
                        "Discount : " + Discount + "Number : " + Number + "Time : " + Time +
                        "Price : " + Price);
                ProductModels product = new ProductModels();
                product.setId(id);
                product.setTitle(Title);
                product.setServiceName(ServiceName);
                product.setDescription(Description);
                product.setDescriptionFull(DescriptionFull);
                product.setDiscount(Discount);
                product.setNumber(Number);
                product.setTime(Time);
                product.setPrice(Price);
                product.setAvatarURL(Avatar);
                InitCommunication.addMohtava(product);
                adapterRecyclerView();
            }
        });
    }

    public void massengersParserAndSet(String link) {
        AsyncHttpClient client = new AsyncHttpClient();

        client.post(link, new TextHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                PublicMethods.toast(context, getString(R.string.searchNotFound));
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                Gson gson = new Gson();
                MessengersModels models = gson.fromJson(responseString, MessengersModels.class);
                int id = models.getId();
                String Title = models.getTitle();
                String MessengerName = models.getMessengerName();
                String Description = models.getDescription();
                String DescriptionFull = models.getDescriptionFull();
                String Discount = models.getDiscount();
                String AvatarURL = models.getAvatarURL();
                String Price = models.getPrice();
                MessengersModels messenger = new MessengersModels();
                messenger.setId(id);
                messenger.setTitle(Title);
                messenger.setMessengerName(MessengerName);
                messenger.setDescription(Description);
                messenger.setDescriptionFull(DescriptionFull);
                messenger.setDiscount(Discount);
                messenger.setAvatarURL(AvatarURL);
                messenger.setPrice(Price);
                communication.addMessenger(messenger);
                adapterRecyclerView();
            }
        });
    }

    public void emailParserAndSet(String link) {
        AsyncHttpClient client = new AsyncHttpClient();

        client.post(link, new TextHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                PublicMethods.toast(context, getString(R.string.searchNotFound));
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                Gson gson = new Gson();
                EmailModels models = gson.fromJson(responseString, EmailModels.class);
                int id = models.getId();
                String Title = models.getTitle();
                String NameBank = models.getNameBank();
                String Description = models.getDescription();
                String Number = models.getNumber();
                double Min = models.getMin();
                double Max = models.getMax();
                double Result = models.getResult();
                String Avatar = models.getAvatarURL();
                EmailModels email = new EmailModels();
                email.setId(id);
                email.setTitle(Title);
                email.setNameBank(NameBank);
                email.setDescription(Description);
                email.setNumber(Number);
                email.setMin(Min);
                email.setMax(Max);
                email.setResult(Result);
                email.setAvatarURL(Avatar);
                InitCommunication.addEmail(email);
                adapterRecyclerView();
            }
        });
    }

    public void pocketParserAndSet(String link) {
        AsyncHttpClient client = new AsyncHttpClient();

        client.post(link, new TextHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                PublicMethods.toast(context, getString(R.string.searchNotFound));
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                Gson gson = new Gson();
                PocketModels models = gson.fromJson(responseString, PocketModels.class);
                int id = models.getId();
                String Title = models.getTitle();
                String NameSize = models.getNameSize();
                String Description = models.getDescription();
                double Min = models.getMin();
                double Max = models.getMax();
                double Result = models.getResult();
                String Avatar = models.getAvatarURL();
                PocketModels pocket = new PocketModels();
                pocket.setId(id);
                pocket.setTitle(Title);
                pocket.setNameSize(NameSize);
                pocket.setDescription(Description);
                pocket.setMin(Min);
                pocket.setMax(Max);
                pocket.setResult(Result);
                pocket.setAvatarURL(Avatar);
                InitCommunication.addPocket(pocket);
                adapterRecyclerView();
            }
        });
    }
}



