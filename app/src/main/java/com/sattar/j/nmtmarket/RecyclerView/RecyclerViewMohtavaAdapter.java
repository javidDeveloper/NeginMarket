package com.sattar.j.nmtmarket.RecyclerView;

//import android.support.v7.app.AlertController;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.sattar.j.nmtmarket.InnerPage.InnerPageActivity;
import com.sattar.j.nmtmarket.Models.ProductModels;
import com.sattar.j.nmtmarket.MyWidget.MyTextView;
import com.sattar.j.nmtmarket.PublicMethods;
import com.sattar.j.nmtmarket.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class RecyclerViewMohtavaAdapter extends RecyclerView.Adapter<RecyclerViewMohtavaAdapter.MyViewHolder> {
    private Context mContext;
    private List<ProductModels> models;
    private View view;


    public RecyclerViewMohtavaAdapter() {
    }

    public RecyclerViewMohtavaAdapter(Context mContext, List<ProductModels> models) {
        this.mContext = mContext;
        this.models = models;

    }

    @Override
    public RecyclerViewMohtavaAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = LayoutInflater.from(mContext).inflate(R.layout.adapter_card_view_mohtava, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerViewMohtavaAdapter.MyViewHolder holder, final int position) {

        holder.description.setText(models.get(position).getDescription());
        Picasso.with(mContext).load(models.get(position).getAvatarURL()).into(holder.avatar);
        double priceVal = Double.parseDouble(models.get(position).getPrice());
        double discountVal = Double.parseDouble(models.get(position).getDiscount());
        if (priceVal > discountVal) {
            holder.price.setText(PublicMethods.currencyToman(models.get(position).getDiscount()) + " " + mContext.getString(R.string.toman));
            holder.Discount.setText(PublicMethods.currencyToman(models.get(position).getPrice()) + " " + mContext.getString(R.string.toman));

        } else {
            holder.price.setText(PublicMethods.currencyToman(models.get(position).getPrice()) + " " + mContext.getString(R.string.toman));
            holder.Discount.setText("");
        }
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PublicMethods.goToExtra(mContext, InnerPageActivity.class, position + 1, mContext.getString(R.string.mohtavaString));
            }
        });
    }

    @Override
    public int getItemCount() {
        return models.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView avatar;
        MyTextView description;
        MyTextView Discount;
        MyTextView price;
        CardView cardView;

        public MyViewHolder(View itemView) {
            super(itemView);
            avatar = itemView.findViewById(R.id.avatar);
            description = itemView.findViewById(R.id.description);
            Discount = itemView.findViewById(R.id.Discount);
            price = itemView.findViewById(R.id.price);
            cardView = itemView.findViewById(R.id.cardView);
        }
    }
}
