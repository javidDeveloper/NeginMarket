//package com.sattar.j.nmtmarket;
//
//import android.content.Context;
//import android.support.v7.app.AppCompatActivity;
//import android.os.Bundle;
//import android.support.v7.widget.GridLayoutManager;
//import android.support.v7.widget.RecyclerView;
//import android.util.Log;
//
//import com.google.gson.Gson;
//import com.loopj.android.http.AsyncHttpClient;
//import com.loopj.android.http.TextHttpResponseHandler;
//import com.sattar.j.nmtmarket.Models.EmailModels;
//import com.sattar.j.nmtmarket.Models.MessengersModels;
//import com.sattar.j.nmtmarket.Models.PocketModels;
//import com.sattar.j.nmtmarket.Models.ProductModels;
//import com.sattar.j.nmtmarket.RecyclerView.RecyclerViewGroupingAdapter;
//import com.sattar.j.nmtmarket.RecyclerView.RecyclerViewMohtavaAdapter;
//import com.sattar.j.nmtmarket.demo.InitCommunication;
//
//import cz.msebera.android.httpclient.Header;
//
//public class MoreActivity extends AppCompatActivity {
//
//    public String cate = "";
//    RecyclerView moreItem;
//    InitCommunication communication = ((InitCommunication) this.getApplication());
//    public static Context context;
//
//    @Override
//    protected void onPause() {
//        super.onPause();
//        InitCommunication.pauseHappend = true;
//        Log.i("_onPause", "22222222");
//    }
//
//    @Override
//    protected void onResume() {
//        super.onResume();
//        Log.i("_onResume", "111111111");
//
//        RecyclerViewAdapters();
//    }
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_more);
//        cate = getIntent().getExtras().getString("title");
//        onBind();
//        RecyclerViewAdapters();
//
//
//    }
//
//    private void onBind() {
//        moreItem =findViewById(R.id.moreRecycler);
//    }
//
//    private void RecyclerViewAdapters() {
//        GridLayoutManager gridLayout = new GridLayoutManager(context, 3);
//
//        if (  cate.toString()=="mohtava") {
//            selectMohtava();
//            RecyclerViewMohtavaAdapter adapterMohtava = new
//                    RecyclerViewMohtavaAdapter(context, InitCommunication.mohtava);
//            moreItem.setAdapter(adapterMohtava);
//            moreItem.setLayoutManager(gridLayout);
//        }
//        if (cate.toString()=="EmailBank"  ) {
//            selectEmail();
//            RecyclerViewGroupingAdapter adapterEmail = new
//                    RecyclerViewGroupingAdapter(context, InitCommunication.email);
//            moreItem.setAdapter(adapterEmail);
//            moreItem.setLayoutManager(gridLayout);
//        }
//        if (cate.toString()=="messengers"  ) {
//            selectMassengers();
//            selectMohtava();
//            RecyclerViewMohtavaAdapter adapterMohtava = new
//                    RecyclerViewMohtavaAdapter(context, InitCommunication.mohtava);
//            moreItem.setAdapter(adapterMohtava);
//            moreItem.setLayoutManager(gridLayout);
//        }
//        if (cate.toString() =="pocket"  ) {
//            selectPocket();
//        }
//
//
//    }
//
//    private void selectMohtava() {
//        String link = getString(R.string.DomainString) + "5a9e70453000007700234c1d";
//        mohtavaParserAndSet(link);
//    }
//
//    private void selectMassengers() {
//        String link = getString(R.string.DomainString) + "5a9d5c4a310000491dab53ca";
//        massengersParserAndSet(link);
//    }
//
//    private void selectEmail() {
//        String link = getString(R.string.DomainString) + "5a9f8c472e00002a0074d0d2";
//        emailParserAndSet(link);
//    }
//
//    private void selectPocket() {
//        String link = getString(R.string.DomainString) + "5a9f950b2e00005a0074d0ec";
//        pocketParserAndSet(link);
//    }
//
//    public void mohtavaParserAndSet(String link) {
//        AsyncHttpClient client = new AsyncHttpClient();
//        client.post(link, new TextHttpResponseHandler() {
//            @Override
//            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
//                PublicMethods.toast(getString(R.string.searchNotFound));
//                Log.e("_onFailure", responseString);
//            }
//            @Override
//            public void onSuccess(int statusCode, Header[] headers, final String responseString) {
//                Gson gson = new Gson();
//                ProductModels models = gson.fromJson(responseString, ProductModels.class);
//                int id = models.getId();
//                String Title = models.getTitle();
//                String ServiceName = models.getServiceName();
//                String Description = models.getDescription();
//                String DescriptionFull = models.getDescriptionFull();
//                String Discount = models.getDiscount();
//                String Number = models.getNumber();
//                String Time = models.getTime();
//                String Price = models.getPrice();
//                String Avatar = models.getAvatarURL();
//                ProductModels product = new ProductModels();
//                product.setId(id);
//                product.setTitle(Title);
//                product.setServiceName(ServiceName);
//                product.setDescription(Description);
//                product.setDescriptionFull(DescriptionFull);
//                product.setDiscount(Discount);
//                product.setNumber(Number);
//                product.setTime(Time);
//                product.setPrice(Price);
//                product.setAvatarURL(Avatar);
//                InitCommunication.addMohtava(product);
//                RecyclerViewAdapters();
//            }
//        });
//    }
//
//    public void massengersParserAndSet(String link) {
//        AsyncHttpClient client = new AsyncHttpClient();
//
//        client.post(link, new TextHttpResponseHandler() {
//
//            @Override
//            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
//                PublicMethods.toast(getString(R.string.searchNotFound));
//            }
//
//            @Override
//            public void onSuccess(int statusCode, Header[] headers, String responseString) {
//                Gson gson = new Gson();
//                MessengersModels models = gson.fromJson(responseString, MessengersModels.class);
//                int id = models.getId();
//                String Title = models.getTitle();
//                String MessengerName = models.getMessengerName();
//                String Description = models.getDescription();
//                String DescriptionFull = models.getDescriptionFull();
//                String Discount = models.getDiscount();
//                String AvatarURL = models.getAvatarURL();
//                String Price = models.getPrice();
//                MessengersModels messenger = new MessengersModels();
//                messenger.setId(id);
//                messenger.setTitle(Title);
//                messenger.setMessengerName(MessengerName);
//                messenger.setDescription(Description);
//                messenger.setDescriptionFull(DescriptionFull);
//                messenger.setDiscount(Discount);
//                messenger.setAvatarURL(AvatarURL);
//                messenger.setPrice(Price);
//                communication.addMessenger(messenger);
//                RecyclerViewAdapters();
//            }
//        });
//    }
//
//    public void emailParserAndSet(String link) {
//        AsyncHttpClient client = new AsyncHttpClient();
//
//        client.post(link, new TextHttpResponseHandler() {
//
//            @Override
//            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
//                PublicMethods.toast(getString(R.string.searchNotFound));
//            }
//
//            @Override
//            public void onSuccess(int statusCode, Header[] headers, String responseString) {
//                Gson gson = new Gson();
//                EmailModels models = gson.fromJson(responseString, EmailModels.class);
//                int id = models.getId();
//                String Title = models.getTitle();
//                String NameBank = models.getNameBank();
//                String Description = models.getDescription();
//                String Number = models.getNumber();
//                double Min = models.getMin();
//                double Max = models.getMax();
//                double Result = models.getResult();
//                String Avatar = models.getAvatarURL();
//                EmailModels email = new EmailModels();
//                email.setId(id);
//                email.setTitle(Title);
//                email.setNameBank(NameBank);
//                email.setDescription(Description);
//                email.setNumber(Number);
//                email.setMin(Min);
//                email.setMax(Max);
//                email.setResult(Result);
//                email.setAvatarURL(Avatar);
//                InitCommunication.addEmail(email);
//                RecyclerViewAdapters();
//            }
//        });
//    }
//
//    public void pocketParserAndSet(String link) {
//        AsyncHttpClient client = new AsyncHttpClient();
//
//        client.post(link, new TextHttpResponseHandler() {
//
//            @Override
//            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
//                PublicMethods.toast(getString(R.string.searchNotFound));
//            }
//
//            @Override
//            public void onSuccess(int statusCode, Header[] headers, String responseString) {
//                Gson gson = new Gson();
//                PocketModels models = gson.fromJson(responseString, PocketModels.class);
//                int id = models.getId();
//                String Title = models.getTitle();
//                String NameSize = models.getNameSize();
//                String Description = models.getDescription();
//                double Min = models.getMin();
//                double Max = models.getMax();
//                double Result = models.getResult();
//                String Avatar = models.getAvatarURL();
//                PocketModels pocket = new PocketModels();
//                pocket.setId(id);
//                pocket.setTitle(Title);
//                pocket.setNameSize(NameSize);
//                pocket.setDescription(Description);
//                pocket.setMin(Min);
//                pocket.setMax(Max);
//                pocket.setResult(Result);
//                pocket.setAvatarURL(Avatar);
//                InitCommunication.addPocket(pocket);
//                RecyclerViewAdapters();
//            }
//        });
//    }
//
//}
