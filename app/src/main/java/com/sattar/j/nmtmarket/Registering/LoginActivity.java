package com.sattar.j.nmtmarket.Registering;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Toast;

import com.sattar.j.nmtmarket.MainActivity;
import com.sattar.j.nmtmarket.MyWidget.MyButton;
import com.sattar.j.nmtmarket.MyWidget.MyEditText;
import com.sattar.j.nmtmarket.MyWidget.MyTextView;
import com.sattar.j.nmtmarket.PublicMethods;
import com.sattar.j.nmtmarket.R;
import com.sattar.j.nmtmarket.ShopActivity;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    MyTextView title;
    MyEditText EnterMobile, Password;
    CheckBox viewPassWord;
    Context mContext = LoginActivity.this;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        onBind();
        onOparation();
    }
    private void onBind() {
        title = findViewById(R.id.title);
        viewPassWord = findViewById(R.id.viewPassWord);
        EnterMobile = findViewById(R.id.EnterMobile);
        Password = findViewById(R.id.Password);
        findViewById(R.id.LoginInNMT).setOnClickListener(this);
        findViewById(R.id.back).setOnClickListener(this);
        findViewById(R.id.Basket).setOnClickListener(this);
        findViewById(R.id.Register).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();

        if (id == R.id.LoginInNMT) {
            if (EnterMobile.getText().toString().equals("")) {
                PublicMethods.toast(this, getString(R.string.pleaseEnterMobileNumber));
            }
            if (!EnterMobile.getText().toString().startsWith("9")) {
                PublicMethods.toast(this, getString(R.string.formatNotAccept));
            }
            if (EnterMobile.getText().toString().length() < 10) {
                PublicMethods.toast(this, getString(R.string.formatNotAccept));
            }
            if (Password.getText().toString().length() < 4) {
                PublicMethods.toast(this, getString(R.string.passwordInputFiveCharakter));

        }else {
                if(EnterMobile.getText().toString().equals("9178516035")
                        && Password.getText().toString().equals("1234"));
                PublicMethods.toast(this, getString(R.string.welcome));
                PublicMethods.goToWhithOneKey(this,MainActivity.class,"Login","true");
            }
        }
        if (id == R.id.back) {
            finish();
        }
        if (id == R.id.Basket) {
            PublicMethods.goTo(this,ShopActivity.class);
        }
        if (id == R.id.Register) {
            PublicMethods.goTo(this,SendCodeActivity.class);
            finish();
        }
    }


    private void onOparation() {
        title.setText(R.string.login);
        viewPassWord.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                PublicMethods.toast(mContext,Password.getText().toString());
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
