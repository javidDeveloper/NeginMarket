package com.sattar.j.nmtmarket.RecyclerView;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sattar.j.nmtmarket.InnerPage.InnerPageActivity;
import com.sattar.j.nmtmarket.GroupingPages.Models.GroupingModel;
import com.sattar.j.nmtmarket.MyWidget.MyTextView;
import com.sattar.j.nmtmarket.PublicMethods;
import com.sattar.j.nmtmarket.R;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;


public class RecyclerViewGroupingAdapter extends RecyclerView.Adapter<RecyclerViewGroupingAdapter.MyViewHolder> {
    private Context mContext;
    private List<GroupingModel> models;
    private View view;


    public RecyclerViewGroupingAdapter() {
    }

    public RecyclerViewGroupingAdapter(Context mContext, List<GroupingModel> models) {
        this.mContext = mContext;
        this.models = models;
        notifyDataSetChanged();
    }

    @Override
    public RecyclerViewGroupingAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = LayoutInflater.from(mContext).inflate(R.layout.adapter_card_view_groping, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerViewGroupingAdapter.MyViewHolder holder, final int position) {

        holder.txtGrouping.setText(models.get(position).getText());

        Picasso.with(mContext).load(models.get(position).getImage()).into(holder.imgGrouping);
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                GroupingModel model= new GroupingModel();
                PublicMethods.goToExtra(mContext, InnerPageActivity.class, position + 1, "mohtava");
            }
        });
    }

    @Override
    public int getItemCount() {
        return models.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        CircleImageView imgGrouping;
        MyTextView txtGrouping;
        CardView cardView;

        public MyViewHolder(View itemView) {
            super(itemView);
            imgGrouping = itemView.findViewById(R.id.imgGrouping);
            txtGrouping = itemView.findViewById(R.id.txtGrouping);
            cardView = itemView.findViewById(R.id.cardView);
        }
    }
}
