package com.sattar.j.nmtmarket.GroupingPages;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.sattar.j.nmtmarket.BaseActivity;
import com.sattar.j.nmtmarket.MyWidget.MyTextView;
import com.sattar.j.nmtmarket.PublicMethods;
import com.sattar.j.nmtmarket.R;
import com.sattar.j.nmtmarket.ShopActivity;


public class ProductCategorizationActivity extends AppCompatActivity implements View.OnClickListener {
    ViewPager GroupingPager;
    Context mContext;
    TabLayout tabs;
    MyTextView title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_categorization);
        onBind();
        onOperation();
        adapterViewPager();
    }

    private void onBind() {
        title = findViewById(R.id.title);
        GroupingPager = findViewById(R.id.GroupingPager);
        tabs = findViewById(R.id.tabs);
        findViewById(R.id.back).setOnClickListener(this);
        findViewById(R.id.Basket).setOnClickListener(this);
    }

    private void onOperation() {
        //**///**//**//**// get title Page
        Intent intent = getIntent();
        String titleVal = intent.getStringExtra("title");
        title.setText(titleVal);
        }

    private void adapterViewPager() {
        MyPagerAdapter adapter = new MyPagerAdapter(getSupportFragmentManager(), mContext);
        GroupingPager.setAdapter(adapter);

        tabs.setupWithViewPager(GroupingPager);
//        springIndicator.setTitles("شبکه های اجتماعی","تولید محتوا","بانک ایمیل مشاغل","پاکت پلیمری");
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.back) {
            finish();
        }
        if (id == R.id.Basket) {
            PublicMethods.moveToActivity(this, ShopActivity.class);
        }
    }
}
