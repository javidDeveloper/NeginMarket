package com.sattar.j.nmtmarket.GroupingPages.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;
import com.sattar.j.nmtmarket.GroupingPages.Models.GroupingModel;
import com.sattar.j.nmtmarket.PublicMethods;
import com.sattar.j.nmtmarket.R;
import com.sattar.j.nmtmarket.RecyclerView.RecyclerViewGroupingAdapter;
import com.sattar.j.nmtmarket.demo.InitCommunication;

import cz.msebera.android.httpclient.Header;

public class SocialNetworksFragment extends Fragment {
    RecyclerView Recycler;
    View v;

    public static SocialNetworksFragment socialNetworks;

    public static SocialNetworksFragment newInstance() {
        if (socialNetworks == null)
            socialNetworks = new SocialNetworksFragment();
        return socialNetworks;
    }

    @Override
    public void onResume() {
        super.onResume();
        getModel();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.e("onCreateView()", "SocialNetworksFragment");
        View view = inflater.inflate(R.layout.social_tab_fragment, container, false);
        Recycler = view.findViewById(R.id.SocialNetworksRecycler);
//        if (!InitCommunication.grouping.isEmpty()) {
            InitCommunication.grouping.clear();
            getModel();
//        }
        setAdapter();
        return view;
    }

    private void getModel() {

        String url = getString(R.string.DomainString) + "5accb44f3200006d00776496";
        AsyncHttpClient httpClient = new AsyncHttpClient();
        httpClient.post(url, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                PublicMethods.toast(getActivity(), getString(R.string.searchNotFound));
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                Gson gson = new Gson();
                GroupingModel model = gson.fromJson(responseString, GroupingModel.class);
                int id = model.getId();
                String Title = model.getTitle();
                String Text = model.getText();
                String URL = model.getImage();
                Log.i("Grouping", "Title: " + Title + "Text: " + Text + "URL: " + URL);
                GroupingModel groupingModel = new GroupingModel();
                groupingModel.setId(id);
                groupingModel.setTitle(Title);
                groupingModel.setText(Text);
                groupingModel.setImage(URL);
                InitCommunication.addGrouping(groupingModel);
                setAdapter();
            }
        });
    }

    private void setAdapter() {
        RecyclerViewGroupingAdapter adapter = new RecyclerViewGroupingAdapter(getActivity(),
                InitCommunication.getGrouping());
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        Recycler.setAdapter(adapter);
        Recycler.setLayoutManager(layoutManager);

    }
}

