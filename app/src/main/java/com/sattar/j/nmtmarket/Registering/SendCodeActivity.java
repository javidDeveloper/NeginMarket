package com.sattar.j.nmtmarket.Registering;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.sattar.j.nmtmarket.MyWidget.MyEditText;
import com.sattar.j.nmtmarket.MyWidget.MyTextView;
import com.sattar.j.nmtmarket.PublicMethods;
import com.sattar.j.nmtmarket.R;
import com.sattar.j.nmtmarket.ShopActivity;

public class SendCodeActivity extends AppCompatActivity implements View.OnClickListener {
    MyTextView title;
    MyEditText EnterMobile, Password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_code);
        onBind();
        selectTitle();
    }

    private void onBind() {
        title = findViewById(R.id.title);
        EnterMobile = findViewById(R.id.EnterMobile);
        Password = findViewById(R.id.Password);
        findViewById(R.id.RegisterInNMT).setOnClickListener(this);
        findViewById(R.id.back).setOnClickListener(this);
        findViewById(R.id.Basket).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();

        if (id == R.id.RegisterInNMT) {
            if (EnterMobile.getText().toString().equals("")) {
                PublicMethods.toast(this, getString(R.string.pleaseEnterMobileNumber));
            }
            if (!EnterMobile.getText().toString().startsWith("9")) {
                PublicMethods.toast(this, getString(R.string.formatNotAccept));
            }
            if (EnterMobile.getText().toString().length() < 10) {
                PublicMethods.toast(this, getString(R.string.formatNotAccept));
            }
            if (Password.getText().toString().length() < 4) {
                PublicMethods.toast(this, getString(R.string.passwordInputFiveCharakter));
            } else {
                String tel = EnterMobile.getText().toString();
                PublicMethods.goToWhithOneKey(this, InputCodeActivity.class, "mobile", "true" + tel);
                finish();
            }
        }
        if (id == R.id.back) {
            finish();
        }
        if (id == R.id.Basket) {
            PublicMethods.goTo(this, ShopActivity.class);
        }
    }


    private void selectTitle() {
        title.setText(getString(R.string.RegisterInNMT));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
