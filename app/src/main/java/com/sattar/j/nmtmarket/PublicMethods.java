package com.sattar.j.nmtmarket;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.View;
import android.widget.Toast;

import java.text.NumberFormat;

public class PublicMethods extends MainActivity {
    Context mContext;

    public static void snack(String s, View v) {
        Snackbar.make(v, s, Snackbar.LENGTH_SHORT).show();
    }

    public static void toast(Context context,String s) {
        Toast.makeText(context, s, Toast.LENGTH_SHORT).show();
    }

    public static void drawerCheck(DrawerLayout drawerLayout) {
        if (drawerLayout.isDrawerOpen(Gravity.RIGHT)) {
            drawerLayout.closeDrawer(Gravity.RIGHT);

        }
    }

    public static String currencyToman(String currency) {
        NumberFormat format = NumberFormat.getNumberInstance();
        long num = Long.parseLong(currency.toString());
        currency = format.format(num/10);
        return currency;
    }

    public static void moveToActivity(Context context , Class toActivity){
        Intent intent = new Intent(context,toActivity);
        context.startActivity(intent);
    }
    public static void goToExtra(Context context,Class goTo, int id, String title) {
        Intent intent = new Intent(context,goTo);
        intent.putExtra("title", title);
        intent.putExtra("id", id);
        context.startActivity(intent);
    }
    public static void goTo(Context context,Class goTo) {
        Intent intent = new Intent(context,goTo);
        context.startActivity(intent);
    }
    public static void goToWhithOneKey(Context context,Class goTo,String key, String value) {
        Intent intent = new Intent(context,goTo);
        intent.putExtra(key, value);
        context.startActivity(intent);
    }
}
